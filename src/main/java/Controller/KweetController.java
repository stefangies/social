package Controller;

import Service.KweetService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/kweets")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class KweetController {

    @Inject
    KweetService service;

    @GET
    public Response getKweets()
    {
        try {
            return Response.ok(service.findAll()).build();
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
            return Response.status(403).build();
        }
    }
}
