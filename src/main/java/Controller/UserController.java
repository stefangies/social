package Controller;

import Model.Kweet.Kweet;
import Model.User.User;
import Service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    UserService service;

    @GET
    public Response getUsers() {
        try {
            return Response.ok(service.findAll()).build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(403).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getUser(
            @PathParam("id") String id,
            @QueryParam("params") List<String> params
    ) {
        try {
            return Response.ok(service.findOne(id, params)).build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(404).entity("User not found").build();
        }
    }

    @PUT
    public Response updateUser(User user) {
        try {
            return Response.ok(service.update(user)).build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(404).entity("User not found").build();
        }
    }


    @POST
    public Response addUser(User user) {
        try {
            service.create(user);
            return Response.status(200).entity("Message" + user + "added successfully").build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(404).entity("Error processing the request").build();
        }
    }

    @POST
    @Path("/{id}/kweets")
    public Response addKweet(
            @PathParam("id") String id,
            Kweet kweet) {
        try {
            service.kweet(id, kweet);
            return Response.ok("Kweet added successfully").build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(404).entity("Error processing the request").build();
        }
    }

    @POST
    @Path("/{id}/followers")
    public Response addFollower(
            @PathParam("id") String id,
            @QueryParam("follower_id") String followerId) {
        try {
            service.follow(id, followerId);
            return Response.ok("Friend successfully added").build();
        } catch (Exception e1) {
            e1.printStackTrace();
            return Response.status(403).entity("Error processing the request").build();
        }
    }
}
