package Database.Implementation.JPA;

import Database.Interface.IDAO;
import Database.Interface.JPAQualifier;
import Model.Model;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

@Stateless
@JPAQualifier
public class JPADAO<T extends Model> implements IDAO<T> {

    @PersistenceContext(name = "PU")
    private EntityManager em;

    public JPADAO() {
    }

    @Override
    public T findOne(Class<T> tClass, String id) {
        return em.find(tClass, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<T> findAll(Class<T> tClass) {
        Query q = em.createQuery("SELECT e FROM " + tClass.getName() + " e");
        return q.getResultList();
    }

    @Override
    public T create(T entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public T update(T entity) {
        return em.merge(entity);
    }

    @Override
    public void delete(String entityId) {
        em.remove(entityId);
    }


}
