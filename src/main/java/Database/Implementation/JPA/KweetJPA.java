package Database.Implementation.JPA;

import Database.Interface.IKweetDAO;
import Database.Interface.JPAQualifier;
import Model.Kweet.Kweet;

import javax.ejb.Stateless;

@Stateless
@JPAQualifier
public class KweetJPA extends JPADAO<Kweet> implements IKweetDAO {
}
