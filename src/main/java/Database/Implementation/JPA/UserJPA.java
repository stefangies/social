package Database.Implementation.JPA;

import Database.Interface.IUserDAO;
import Database.Interface.JPAQualifier;
import Model.User.User;

import javax.ejb.Stateless;

@Stateless
@JPAQualifier
public class UserJPA extends JPADAO<User> implements IUserDAO {
}
