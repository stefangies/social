package Database.Implementation.Memory;

import Database.Interface.IKweetDAO;
import Database.Interface.MemoryQualifier;
import Model.Kweet.Kweet;

import javax.ejb.Singleton;

@Singleton
@MemoryQualifier
public class KweetMemory extends MemoryDAO<Kweet> implements IKweetDAO {
}
