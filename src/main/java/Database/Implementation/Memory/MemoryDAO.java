package Database.Implementation.Memory;

import Database.Interface.IDAO;
import Database.Interface.MemoryQualifier;
import Model.Model;

import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.Collection;

//@Stateless
@MemoryQualifier
@Singleton
public class MemoryDAO<T extends Model> implements IDAO<T> {

    private Collection<T> tCollection = new ArrayList<>();

    @Override
    public T findOne(Class<T> tClass, String id) {
        return null;
    }

    @Override
    public Collection<T> findAll(Class<T> tClass) {
        return tCollection;
    }

    @Override
    public T create(T entity) {
        tCollection.add(entity);
        return entity;
    }

    @Override
    public T update(T entity) {
        return null;
    }

    @Override
    public void delete(String entityId) {

    }
}
