package Database.Implementation.Memory;

import Database.Interface.IUserDAO;
import Database.Interface.MemoryQualifier;
import Model.User.User;

import javax.ejb.Singleton;

@Singleton
@MemoryQualifier
public class UserMemory extends MemoryDAO<User> implements IUserDAO {
}
