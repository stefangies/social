package Database.Interface;

import java.util.Collection;

public interface IDAO<T> {

    T findOne(Class<T> tClass, String id);

    Collection<T> findAll(Class<T> tClass);

    T create(T entity);

    T update(T entity);

    void delete(String entityId);
}
