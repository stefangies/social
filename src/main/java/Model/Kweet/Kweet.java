package Model.Kweet;

import Model.Model;
import Model.User.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "kweets")
public class Kweet extends Model {

    public String message;

    @ManyToMany(mappedBy = "kweets")
    private List<User> kweets;

    public Kweet() {
    }

    public Kweet(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setKweets(List<User> kweets) {
        this.kweets = kweets;
    }
}
