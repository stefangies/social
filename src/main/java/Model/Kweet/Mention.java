package Model.Kweet;

import Model.User.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mentions")
public class Mention extends Tag {

    @Id
    public String userId;

    @ManyToOne
    private User user;

    public Mention() {
    }
}
