package Model.Kweet;

import Model.Model;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "tags")
public abstract class Tag extends Model {

    public String characterLocation;

    public Tag() {
    }
}
