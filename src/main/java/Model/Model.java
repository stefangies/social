package Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class Model {

    @Id
    private String id;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    Date createdAt;

    @PrePersist
    public void prePersist()
    {
        this.id = UUID.randomUUID().toString();
    }


    public String getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
