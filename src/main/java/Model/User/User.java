package Model.User;

import Model.Kweet.Kweet;
import Model.Kweet.Mention;
import Model.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "users")
public class User extends Model implements Serializable {

    private String userName;
    private String password;
    private String bio;
    private String website;
    private byte[] picture;
    private String email;

    public User() {
    }

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL
    )
    public List<Mention> mentions;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_kweets",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "kweet_id")
    )
    public List<Kweet> kweets;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kweet_id")
    private Kweet liked;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "follower", joinColumns = @JoinColumn(
            name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
            name = "follower_id", referencedColumnName = "id"))
    public List<User> followers;

    @ManyToMany(mappedBy = "followers")
    public List<User> following;

    public List<User> getFollowers() {
        return followers;
    }

    public List<User> getFollowing() {
        return following;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Mention> getMentions() {
        return mentions;
    }

    public void setMentions(List<Mention> mentions) {
        this.mentions = mentions;
    }

    public List<Kweet> getKweets() {
        return kweets;
    }

    public void addKweet(Kweet kweet) {
        this.kweets.add(kweet);
    }

    public void addFolower(User user) {
        this.followers.add(user);
    }

    public void addFollowing(User user)
    {
        this.following.add(user);
    }

    public Kweet getLiked() {
        return liked;
    }

    public void setLiked(Kweet liked) {
        this.liked = liked;
    }
}
