package Service;

import Database.Interface.IKweetDAO;
import Database.Interface.JPAQualifier;
import Model.Kweet.Kweet;

import javax.inject.Inject;
import java.util.Collection;

public class KweetService {

    @Inject
//    @MemoryQualifier
    @JPAQualifier
    private IKweetDAO iKweetDAO;

    public Kweet findOne(String id) {
        return iKweetDAO.findOne(Kweet.class, id);
    }

    public Collection<Kweet> findAll()
    {
        return iKweetDAO.findAll(Kweet.class);
    }

    public void create(Kweet entity) {
        iKweetDAO.create(entity);
    }

    public Kweet update(Kweet entity) {
        return iKweetDAO.update(entity);
    }

    public void delete(String entityId) {
        iKweetDAO.delete(entityId);
    }

    public void save(Kweet object)
    {
        iKweetDAO.create(object);
    }
}
