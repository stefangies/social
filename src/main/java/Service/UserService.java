package Service;

import Database.Interface.IUserDAO;
import Database.Interface.JPAQualifier;
import Model.Kweet.Kweet;
import Model.User.User;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

//@Stateless
public class UserService {

    @Inject
//    @MemoryQualifier
    @JPAQualifier
    private IUserDAO iUserDAO;

    public User findOne(String id, List<String> params) {
        User user = iUserDAO.findOne(User.class, id);
        if (params.contains("followers")) {
            user.getFollowers();
        }
        if (params.contains("likes")) {
            user.getLiked();
        }
        if (params.contains("kweets")) {
            user.getKweets();
        }
        return user;
    }

    public Collection<User> findAll() {
        return iUserDAO.findAll(User.class);
    }

    public void create(User entity) {
        iUserDAO.create(entity);
    }

    public User update(User entity) {
        return iUserDAO.update(entity);
    }

    public void delete(String entityId) {
        iUserDAO.delete(entityId);
    }

    public void save(User object) {
        iUserDAO.create(object);
    }

    public void follow(String userId, String userIdToFollow) {
        User user = iUserDAO.findOne(User.class, userId);
        User userToFollow = iUserDAO.findOne(User.class, userIdToFollow);
        user.addFollowing(userToFollow);
        userToFollow.addFollowing(user);
        iUserDAO.update(user);
    }

    public void kweet(String id, Kweet kweet)
    {
        User user = iUserDAO.findOne(User.class, id);
        user.addKweet(kweet);
        iUserDAO.update(user);
    }
}
