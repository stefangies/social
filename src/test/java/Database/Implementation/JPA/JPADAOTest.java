package Database.Implementation.JPA;

import Database.Interface.IKweetDAO;
import Database.Interface.JPAQualifier;
import Model.Kweet.Kweet;
import Model.Kweet.Mention;
import Model.User.User;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@RunWith(Arquillian.class)
public class JPADAOTest {

    @Deployment
    public static Archive<WebArchive> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(KweetJPA.class.getPackage())
                .addClasses(JPADAO.class, IKweetDAO.class, Kweet.class, User.class, Mention.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "WEB-INF/beans.xml");
    }

    @PersistenceContext
    EntityManager em;

    @Inject
    @JPAQualifier
    IKweetDAO kweetIDAO;

    @Test
    @InSequence(1)
    public void findOne() {
        Kweet k = kweetIDAO.create(new Kweet("hello"));
        Assert.assertEquals("hello", k.getMessage());
    }

    @Test
    @InSequence(2)
    public void findAll() {
        Collection<Kweet> kweets = kweetIDAO.findAll(Kweet.class);
        Assert.assertEquals(1, kweets.size());
    }

    @Test
    public void create() {
        Kweet k = kweetIDAO.create(new Kweet("hello"));
        Assert.assertEquals("hello", k.getMessage());
    }

    @Test
    public void update() {
        Kweet k = kweetIDAO.findOne(Kweet.class, "id");
        k.setMessage("bonjour");
        Kweet update = kweetIDAO.update(k);
    }

    @Test
    public void delete() {
    }
}